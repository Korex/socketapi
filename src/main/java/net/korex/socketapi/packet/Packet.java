package net.korex.socketapi.packet;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.EOFException;
import java.io.IOException;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.List;

public class Packet {

    private int id;
    private List<PacketData> data;

    public static class PacketData {

        private int id;
        private byte[] value;

        public PacketData(int id, byte[] value) {
            this.id = id;
            this.value = value;
        }

        public void write(DataOutputStream stream) throws IOException {
            stream.writeInt(this.id);
            stream.writeInt(this.value.length);
            for (byte b : this.value) {
                stream.writeByte(b);
            }
        }

    }

    public Packet(int id) {
        this(id, new ArrayList<>());
    }

    public Packet(int id, List<PacketData> data) {
        this.id = id;
        this.data = data;
    }

    public int getId() {
        return id;
    }

    public Packet addField(int keyId, byte[] value) {
        this.data.add(new PacketData(keyId, value));
        return this;
    }

    public byte[] getField(int keyId) {
        for (PacketData data : this.data) {
            if(data.id == keyId) {
                return data.value;
            }
        }

        return null;
    }

    public void write(DataOutputStream stream) throws IOException {
        stream.writeInt(this.id);
        stream.writeInt(this.data.size());

        for (PacketData data : this.data) {
            data.write(stream);
        }
    }

    public List<PacketData> getData() {
        return data;
    }

    public static byte[] toNonPrimitive(Byte[] bytesNon) {
        byte[] bytes = new byte[bytesNon.length];
        for (int i = 0; i < bytesNon.length; i++) {
            Byte b = bytesNon[i];
            bytes[i] = b;
        }
        return bytes;
    }

    public static Packet getIncoming(DataInputStream stream) {
        try {
            int id = stream.readInt();
            int keys = stream.readInt();

            List<PacketData> data = new ArrayList<>();

            for (int i = 0; i < keys; i++) {
                int keyId = stream.readInt();
                int length = stream.readInt();

                byte[] values = new byte[length];
                for (int i1 = 0; i1 < length; i1++) {
                    values[i1] = stream.readByte();
                }

                data.add(new PacketData(keyId, values));
            }

            return new Packet(id, data);
        } catch(IOException e) {
            if(e instanceof EOFException || e instanceof SocketException) {
                return null;
            } else {
                e.printStackTrace();
            }
            return null;
        }

    }

}
