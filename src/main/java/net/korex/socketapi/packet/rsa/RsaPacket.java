package net.korex.socketapi.packet.rsa;

import net.korex.socketapi.packet.Packet;

public class RsaPacket extends Packet {
    public RsaPacket(int id) {
        super(id);
    }

    @Override
    public Packet addField(int keyId, byte[] value) {
        return super.addField(keyId, value);
    }
}
