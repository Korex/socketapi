package net.korex.socketapi.packet.rsa;

import net.korex.socketapi.packet.ApiPackets;
import net.korex.socketapi.packet.Packet;

import java.security.PrivateKey;
import java.security.PublicKey;

public class RsaCipherPacket {

    public static Packet create(byte[] content, PublicKey publicKey) {
        Rsa rsa = new Rsa();
        byte[] encrypted = rsa.encrypt(content, publicKey);
        return new Packet(ApiPackets.RSA_CIPHER).addField(0, encrypted);
    }

    public static byte[] getContent(Packet packet, PrivateKey privateKey) {
        Rsa rsa = new Rsa();
        byte[] content = packet.getField(0);
        byte[] decrypted = rsa.decrypt(content, privateKey);
        return decrypted;
    }

}
