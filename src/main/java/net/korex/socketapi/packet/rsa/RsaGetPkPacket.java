package net.korex.socketapi.packet.rsa;

import net.korex.socketapi.packet.ApiPackets;
import net.korex.socketapi.packet.Packet;

import java.security.PublicKey;

public class RsaGetPkPacket {

    public static Packet create() {
        return new Packet(ApiPackets.RSA_GET_PK);
    }

    public static Packet create(PublicKey publicKey) {
        Packet packet = new Packet(ApiPackets.RSA_GET_PK);
        packet.addField(1, publicKey.getEncoded());
        return packet;
    }

    public static PublicKey getPk(Packet packet) {
        if(packet.getField(1) == null) {
            return null;
        }
        PublicKey key = Rsa.getPublicKey(packet.getField(1));
        return key;
    }

}
