package net.korex.socketapi.packet;

public class ApiPackets {

    public static final int RSA_GET_PK = 691;
    public static final int RSA_CIPHER =  692;
    public static final int INTERRUPTED_DATA = 699;

}
