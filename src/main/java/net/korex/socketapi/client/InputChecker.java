package net.korex.socketapi.client;

import java.io.DataInputStream;

import net.korex.socketapi.CustomThread;
import net.korex.socketapi.Result;

public class InputChecker extends CustomThread {

	private Client client;
	private DataInputStream stream;

	public InputChecker(Client client) {
		this.client = client;
		this.stream = new DataInputStream(this.client.getIn());
	}

	@Override
	public void execute() {
		Result result = new Result(this.stream);
		this.client.receive(result);
	}

}
