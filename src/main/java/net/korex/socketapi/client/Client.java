package net.korex.socketapi.client;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

import net.korex.socketapi.Result;
import net.korex.socketapi.Sendable;
import net.korex.socketapi.packet.Packet;

public abstract class Client extends Sendable {

	private Socket clientSocket;
	private InputChecker inputChecker;

	public Client(String host, int port) {
		super();
		try {
			this.clientSocket = new Socket(host, port);
			this.inputChecker = new InputChecker(this);
			this.inputChecker.start();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public DataInputStream getIn() {try {
		return new DataInputStream(this.clientSocket.getInputStream());
	} catch (IOException e) {
		e.printStackTrace();
	}
		return null;
	}

	@Override
	public DataOutputStream getOut() {
		try {
			return new DataOutputStream(this.clientSocket.getOutputStream());
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	public Socket getClientSocket() {
		return this.clientSocket;
	}

	public InputChecker getInputChecker() {
		return this.inputChecker;
	}

	public void send(Packet packet) {
		try {
			packet.write(this.getOut());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}


	public void close() {
		try {
			this.inputChecker.stop();
			this.clientSocket.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public abstract void receive(Result result);

}
