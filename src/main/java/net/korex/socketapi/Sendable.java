package net.korex.socketapi;

import net.korex.socketapi.packet.Packet;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

public abstract class Sendable {

    public abstract DataInputStream getIn();

    public abstract DataOutputStream getOut();

    public abstract void close();

    public void send(Packet packet) {
        try {
            packet.write(this.getOut());
        } catch (IOException e) {
            try {
                System.out.println("[WARNING] Trying to close OutPutStream cause of IOException in Sendable #22");
                this.getOut().close();
            } catch (IOException e1) {
                e1.printStackTrace();
                System.out.println("[WARNING] Something went wrong! Couldnt close OutPutStream!");
            }

        }

    }

}
