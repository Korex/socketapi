package net.korex.socketapi;

public abstract class CustomThread implements Runnable, Stoppable {

	private Thread thread;
	private boolean run;

	public CustomThread() {
		this.run = false;
	}

	public void start() {
		if (this.thread == null && this.run == false) {
			this.thread = new Thread(this);
			this.run = true;
			this.thread.start();
		}
	}

	@Override
	public void stop() {
		if (this.thread != null && this.run == true) {
			this.run = false;
			this.thread = null;
		}
	}

	@Override
	public void run() {
		while (this.run) {
			this.execute();
		}
	}

	public abstract void execute();

}
