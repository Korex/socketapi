package net.korex.socketapi;

import net.korex.socketapi.packet.ApiPackets;
import net.korex.socketapi.packet.Packet;

import java.io.DataInputStream;
import java.io.IOException;

public class Result {

    private DataInputStream stream;

    public Result(DataInputStream stream) {
        this.stream = stream;
    }

    public Packet getPacket() {
        Packet packet = Packet.getIncoming(this.stream);
        if(packet == null) {
            try {
                this.stream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return new Packet(ApiPackets.INTERRUPTED_DATA);
        }
        return packet;

    }

}
