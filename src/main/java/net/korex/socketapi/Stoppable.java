package net.korex.socketapi;

public interface Stoppable {
	void stop();
}
