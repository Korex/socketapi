package net.korex.socketapi.server;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class SocketThreadManager {

	private ExecutorService exec;
	private Server server;

	public SocketThreadManager(Server server) {
		this.server = server;
		exec = Executors.newFixedThreadPool(this.server.getMaxThreadsAndClients());
	}

	public boolean accept(ClientSocket client) {
		if (this.server.getConnectedSockets().size() > this.server.getMaxThreadsAndClients()) {
			return false;
		}
		SocketThread socketThread = new SocketThread(client, this.server);
		exec.submit(socketThread);
		return true;
	}

}
