package net.korex.socketapi.server;

import java.io.IOException;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

import net.korex.socketapi.CustomThread;

public class ConnectedSocketsManager extends CustomThread {

	private Server server;
	private List<ClientSocket> connectedSockets;

	public ConnectedSocketsManager(Server server) {
		this.server = server;
		this.connectedSockets = new ArrayList<>();
	}

	private void refreshConnectedSockets() {
		List<ClientSocket> connectedSockets = new ArrayList<>();
		for (ClientSocket socket : this.connectedSockets) {
			if (socket.isAvailable()) {
				connectedSockets.add(socket);
			}
		}
		this.connectedSockets = connectedSockets;
	}

	public List<ClientSocket> getConnectedSockets() {
		this.refreshConnectedSockets();
		return this.connectedSockets;
	}

	@Override
	public void execute() {
		try {
			Socket newClient = server.getServerSocket().accept();
			ClientSocket clientSocket = new ClientSocket(newClient);
			if (!this.server.getClientThreadManager().accept(clientSocket)) {
				newClient.close();
				return;
			} 
			this.connectedSockets.add(clientSocket); // add to List
			this.refreshConnectedSockets(); // kick offline players out of list
			this.server.clientConnectEvent(clientSocket); // fire Event

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
