package net.korex.socketapi.server;

import java.io.IOException;
import java.net.ServerSocket;
import java.util.List;

import net.korex.socketapi.Result;

public abstract class Server {

	private ServerSocket serverSocket;
	private SocketThreadManager clientThreadManager;
	private ConnectedSocketsManager connectedSocketsManager;
	private int maxThreadsAndClients;

	public Server(int port, int maxThreadsAndClients) {
		this.maxThreadsAndClients = maxThreadsAndClients;
		try {
			this.serverSocket = new ServerSocket(port);
		} catch (IOException e) {
			e.printStackTrace();
		}
		this.connectedSocketsManager = new ConnectedSocketsManager(this);
		this.connectedSocketsManager.start();
		this.clientThreadManager = new SocketThreadManager(this);
	}

	public List<ClientSocket> getConnectedSockets() {
		return this.connectedSocketsManager.getConnectedSockets();
	}

	public SocketThreadManager getClientThreadManager() {
		return this.clientThreadManager;
	}

	public int getMaxThreadsAndClients() {
		return this.maxThreadsAndClients;
	}

	public ServerSocket getServerSocket() {
		return this.serverSocket;
	}

	public abstract void clientConnectEvent(ClientSocket clientSocket);

	public abstract void recieveEvent(ClientSocket sender, Result result);

}
