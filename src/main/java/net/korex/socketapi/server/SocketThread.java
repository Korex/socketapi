package net.korex.socketapi.server;

import java.io.DataInputStream;

import net.korex.socketapi.Result;
import net.korex.socketapi.Stoppable;

public class SocketThread implements Runnable, Stoppable {
    private ClientSocket client;
    private Server server;
    private boolean run;

    public SocketThread(ClientSocket client, Server server) {
        this.client = client;
        this.server = server;
        this.run = true;
    }

    @Override
    public void stop() {
        this.run = false;
    }

    @Override
    public void run() {
        while (this.run) {
            if (!this.client.isAvailable()) {
                this.client.close();
                this.stop();
                return;
            }
            DataInputStream stream = this.client.getIn();
            this.server.recieveEvent(this.client, new Result(stream));
        }
    }
}
