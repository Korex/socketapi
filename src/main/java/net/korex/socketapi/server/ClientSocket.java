package net.korex.socketapi.server;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;

import net.korex.socketapi.Sendable;
import net.korex.socketapi.packet.Packet;

public class ClientSocket extends Sendable {

    private Socket socket;
    private DataInputStream inputStream;
    private DataOutputStream outputStream;

    public ClientSocket(Socket socket) {
        try {
            this.outputStream = new DataOutputStream(socket.getOutputStream());
            this.inputStream = new DataInputStream(socket.getInputStream());
        } catch (IOException e) {
            e.printStackTrace();
        }

        this.socket = socket;
    }

    public Socket getSocket() {
        return this.socket;
    }

    public InetAddress getInetAddress() {
        return this.socket.getInetAddress();
    }

    public boolean isAvailable() {
        if (this.socket == null || !this.socket.isConnected() || this.socket.isClosed()) {
            return false;
        } else {
            return true;
        }
    }

    @Override
    public DataInputStream getIn() {
        return this.inputStream;
    }

    @Override
    public DataOutputStream getOut() {
        return this.outputStream;
    }

    @Override
    public void close() {
        try {
            socket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


}
