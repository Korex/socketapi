package net.korex.socketapi.example.basitctest;

import net.korex.socketapi.Result;
import net.korex.socketapi.packet.Packet;
import net.korex.socketapi.server.ClientSocket;
import net.korex.socketapi.server.Server;

public class TestServer extends Server {

    public static void main(String[] args) {
        new TestServer();
    }

    public TestServer() {
        super(25565, 20);
        System.out.println("Started Server");
    }

    @Override
    public void clientConnectEvent(ClientSocket clientSocket) {
        System.out.println("(Server) client " + clientSocket.getInetAddress().getHostName() + " connected");
    }

    @Override
    public void recieveEvent(ClientSocket sender, Result result) {
        Packet packet = result.getPacket();
        System.out.println("(Server) received packet: " + packet.getId());
        String message = TestPacket.getMessage(packet);
        System.out.println("(Server) got message: " + message);
        System.out.println("(Server) sending message to client");

        sender.send(TestPacket.create("Die Nachricht kommt vom Server"));
        System.out.println(this.getConnectedSockets().size());
    }
}
