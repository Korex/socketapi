package net.korex.socketapi.example.basitctest;

import net.korex.socketapi.Result;
import net.korex.socketapi.client.Client;
import net.korex.socketapi.packet.Packet;

public class TestClient extends Client {

    public static void main(String[] args) {
        for (int i = 0; i < 23; i++) {
            new TestClient();
            try {
                Thread.sleep(200);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.println("Ready");
    }

    public TestClient() {
        super("localhost", 25565);

        System.out.println("(Client) Sending message to server");
        this.send(TestPacket.create("Die Nachricht kommt vom client"));
    }

    @Override
    public void receive(Result result) {
        Packet packet = result.getPacket();

        System.out.println("(Client) received  packet: " + packet.getId());

        String message = TestPacket.getMessage(packet);
        System.out.println("(Client) got message: " + message);

        //this.close();

    }
}
