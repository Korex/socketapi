package net.korex.socketapi.example.basitctest;

import net.korex.socketapi.packet.Packet;

public class TestPacket {

    public static Packet create(String message) {
        return new Packet(0).addField(0, message.getBytes());
    }

    public static String getMessage(Packet packet) {
        return new String(packet.getField(0));
    }

}
