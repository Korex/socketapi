package net.korex.socketapi.example.rsa;

public class RsaMain {

    public static void main(String[] args) throws InterruptedException {
        new RsaServer();
        Thread.sleep(1000);
        new RsaClient();
    }

}
