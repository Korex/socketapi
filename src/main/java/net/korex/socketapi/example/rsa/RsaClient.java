package net.korex.socketapi.example.rsa;

import net.korex.socketapi.Result;
import net.korex.socketapi.client.Client;
import net.korex.socketapi.packet.ApiPackets;
import net.korex.socketapi.packet.Packet;
import net.korex.socketapi.packet.rsa.RsaCipherPacket;
import net.korex.socketapi.packet.rsa.RsaGetPkPacket;

import java.security.PublicKey;
import java.util.UUID;

public class RsaClient extends Client {
    public RsaClient() {
        super("localhost", 25565);

        System.out.println("(Client) Starting client.");
        System.out.println("(Client) Sending GETPUBLIC packet");

        Packet packet = RsaGetPkPacket.create();
        this.send(packet);

    }

    @Override
    public void receive(Result result) {
        Packet packet = result.getPacket();

        System.out.println("(Client) Received: " + packet.getId());

        if (packet.getId() == ApiPackets.RSA_GET_PK) {
            PublicKey publicKey = RsaGetPkPacket.getPk(packet);
            String message = UUID.randomUUID().toString();
            System.out.println("(Client) Encrypted length: " + message.length());
            System.out.println("(Client) Sending encrypted: " + message);

            Packet send = RsaCipherPacket.create(message.getBytes(), publicKey);

            this.send(send);
        }
    }
}
