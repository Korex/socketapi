package net.korex.socketapi.example.rsa;

import net.korex.socketapi.Result;
import net.korex.socketapi.packet.ApiPackets;
import net.korex.socketapi.packet.Packet;
import net.korex.socketapi.packet.rsa.Rsa;
import net.korex.socketapi.packet.rsa.RsaCipherPacket;
import net.korex.socketapi.packet.rsa.RsaGetPkPacket;
import net.korex.socketapi.server.ClientSocket;
import net.korex.socketapi.server.Server;

import java.security.KeyPair;
import java.security.PrivateKey;
import java.util.HashMap;

public class RsaServer extends Server {

    private HashMap<ClientSocket, KeyPair> keys;

    public RsaServer() {
        super(25565, 10);

        System.out.println("(Server) Starting server.");

        this.keys = new HashMap<>();
    }

    @Override
    public void clientConnectEvent(ClientSocket clientSocket) {
        System.out.println("(Server) Client connected: " + clientSocket.getInetAddress().getHostName());
    }

    @Override
    public void recieveEvent(ClientSocket sender, Result result) {
        Packet packet = result.getPacket();

        System.out.println("(Server) Receivecd packet: " + packet.getId());

        if(packet.getId() == ApiPackets.RSA_GET_PK) {
            KeyPair pair = Rsa.gen(1024);

            this.keys.put(sender, pair);

            Packet publicKeyPacket = RsaGetPkPacket.create(pair.getPublic());
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            sender.send(publicKeyPacket);
        } else if(packet.getId() == ApiPackets.RSA_CIPHER) {
            PrivateKey privateKey = this.keys.get(sender).getPrivate();
            byte[] hiddenContent = RsaCipherPacket.getContent(packet, privateKey);
            System.out.println("(Server) Received secret String: " + new String(hiddenContent));
        }

    }

}
